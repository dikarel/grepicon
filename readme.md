# grepicon

Search and drag icons into Sketch

[![build status](https://gitlab.com/dikarel/grepicon/badges/master/build.svg)](https://gitlab.com/dikarel/grepicon/commits/master)

## Quickstart

    # Install dependencies
    npm install

    # Run project
    npm start

    # Unit test project
    npm test

    # Lint project
    npm run lint

    # Check unit test coverage
    npm run coverage

    # Watch project for changes and run unit tests automatically
    npm run watch

    # Run acceptance tests
    npm run acceptance
