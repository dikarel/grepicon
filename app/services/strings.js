const strings = require('../../assets/strings.json')

module.exports = {get: get}

function get (stringId, params) {
  if (!strings[stringId]) throw new Error(`Unknown string ID: ${stringId}`)
  let string = strings[stringId]

  // Apply params
  if (params) {
    Object.keys(params).forEach(paramName => {
      string = string.replace(`{${paramName}}`, params[paramName])
    })
  }

  return string
}
