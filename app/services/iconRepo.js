const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const path = require('path')
const fontAwesomePath = path.join(__dirname, '..', '..', 'assets', 'font-awesome')

module.exports = {search: search}

function search (query) {
  return fs.readdirAsync(fontAwesomePath)
    .then(filenames => filenames.filter(isSvg))
    .filter(byName(query))
    .map(toIcon)
}

function byName (name) {
  return filename => path.basename(filename, '.svg').indexOf(name) > -1
}

function isSvg (filename) {
  return filename.match(/\.svg$/i)
}

function toIcon (iconFilename) {
  const iconPath = path.join(fontAwesomePath, iconFilename)

  return fs.readFileAsync(iconPath, 'utf8')
    .then(svg => ({
      name: path.basename(iconFilename, '.svg'),
      svg: svg
    }))
}
