const m = require('mithril')

module.exports = {
  controller: controller,
  view: view
}

function controller () {
  return {}
}

function view (vm, {selectedIndex, labels, onView, onSelect}) {
  const selector = m('u-tabs-selector', labels.map((label, i) =>
    m('u-tabs-label', {
      class: (i === selectedIndex) ? 'selected' : '',
      onclick: () => onSelect(i)
    }, label)
  ))

  const content = m('u-tabs-content', onView(selectedIndex))
  return m('u-tabs', [selector, content])
}
