const m = require('mithril')
const browser = require('./browser')

module.exports = {
  controller: controller,
  view: view
}

function controller () { return {} }

function view (vm) {
  return m(browser)
}
