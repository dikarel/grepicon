const iconRepo = require('../services/iconRepo')
const Promise = require('bluebird')
const m = require('mithril')

module.exports = {
  controller: controller,
  view: require('./browser.view')
}

function controller () {
  const vm = {}

  let searchPromise = null
  let searchResults = []
  let searchQuery = ''

  vm.searchResults = (val) => {
    if (val === undefined) {
      return searchResults.map(result => ({
        name: () => result.name.replace(/-/g, ' '),
        thumbnail: () => result.svg
      }))
    }

    searchResults = val
  }

  vm.noResultsMessageVisible = () => {
    return !searchResults.length && !!vm.searchQuery()
  }

  vm.emptyStateMessageVisible = () => {
    return !vm.searchQuery() && !searchResults.length
  }

  vm.searchQuery = function (val) {
    if (val === undefined) return searchQuery
    if (searchPromise) searchPromise.cancel()
    searchQuery = val

    if (!searchQuery) {
      searchResults = []
    } else {
      m.startComputation()
      searchPromise = Promise.delay(300)
        .then(() => iconRepo.search(val))
        .then(res => vm.searchResults(res))
        .catch(console.error)
        .finally(() => m.endComputation())
    }
  }

  return vm
}
