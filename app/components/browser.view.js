const strings = require('../services/strings')
const m = require('mithril')

module.exports = (vm) => {
  return m('g-browser', [
    noResultsMessage(vm),
    emptyStateMessage(vm),
    results(vm),
    searchInput(vm)
  ])
}

function noResultsMessage (vm) {
  if (!vm.noResultsMessageVisible()) return null
  return m('g-message#noResults', strings.get('noResults'))
}

function emptyStateMessage (vm) {
  if (!vm.emptyStateMessageVisible()) return null
  return m('g-message#emptyState', strings.get('emptyState'))
}

function results (vm) {
  const results = vm.searchResults()
  if (!results.length) return null

  return m('g-results', results.map(result =>
    m('g-result', [
      m('a', {href: '../assets/font-awesome/500px.svg', download: '500px.svg'}, m('g-thumbnail', m.trust(result.thumbnail()))),
      m('g-name', result.name())
    ])
  ))
}

function searchInput (vm) {
  return m('g-search', m('input#searchQuery', {
    placeholder: strings.get('placeholder'),
    autofocus: 'autofocus',
    value: vm.searchQuery(),
    oninput: m.withAttr('value', vm.searchQuery)
  }))
}
