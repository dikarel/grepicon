# Contributing

To contribute, create a merge request subject to the requirements below.

## Requirements

- Does not break the build
- Describe what is being changed and why it is necessary
- Subject to approval
