const iconRepo = require('../app/services/iconRepo')

describe('iconRepo', () => {
  describe('search', () => {
    let icons

    beforeEach(() => iconRepo.search('angle').then(res => { icons = res }))

    it('returns a list of icons with matching names', () => {
      const iconNames = icons.map(icon => icon.name)
      expect(iconNames).toContain('angle-double-down')
      expect(iconNames).toContain('angle-double-up')
      expect(iconNames).toNotContain('align-center')
    })

    it('svg is provided with each icon', () => {
      expect(icons[0].svg).toContain('<svg')
    })
  })
})
