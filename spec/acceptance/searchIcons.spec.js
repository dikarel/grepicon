const {Application} = require('spectron')
const Promise = require('bluebird')
const {writeFileAsync} = Promise.promisifyAll(require('fs'))
const mkdirp = Promise.promisify(require('mkdirp'))

describe('search icons', function () {
  this.timeout(20000)
  let app

  before(() => mkdirp('screenshots'))

  beforeEach(() => {
    app = new Application({path: require('electron-prebuilt'), args: ['app/main']})
    return app.start()
  })

  afterEach(() => {
    if (app && app.isRunning()) return app.stop()
  })

  it('shows empty state', () =>
    app.client.isVisible('#emptyState')
      .then((visible) => expect(visible).toBe(true))
  )

  it('screenshot', () =>
    app.browserWindow.capturePage()
      .then(screenshot => writeFileAsync('screenshots/empty-state.png', screenshot))
  )

  describe('when searching without results', () => {
    beforeEach(() =>
      app.client
        .setValue('#searchQuery', 'nonexistent')
        .then(() => Promise.delay(500))
    )

    it('displays the no-results message', () =>
      app.client.isVisible('#noResults')
        .then((visible) => expect(visible).toBeTruthy())
    )

    it('screenshot', () =>
      app.browserWindow.capturePage()
        .then(screenshot => writeFileAsync('screenshots/no-results.png', screenshot))
    )
  })

  describe('when searching with results', () => {
    beforeEach(() =>
      app.client
        .setValue('#searchQuery', 'angle')
        .then(() => Promise.delay(500))
    )

    it('displays results', () =>
      app.client.isVisible('g-result')
        .then((visible) => expect(visible).toBeTruthy())
    )

    it('screenshot', () =>
      app.browserWindow.capturePage()
        .then(screenshot => writeFileAsync('screenshots/search-results.png', screenshot))
    )
  })
})
