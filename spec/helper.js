global.expect = require('expect')
global.sinon = require('sinon')
global.createSpy = expect.createSpy
global.spyOn = expect.spyOn
global.isSpy = expect.isSpy
global.restoreSpies = expect.restoreSpies
global.createSpy = expect.createSpy
global.useFakeTimers = sinon.useFakeTimers
