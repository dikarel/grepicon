const app = require('../app/components/app')

describe('app', () => {
  let vm

  beforeEach(() => { vm = app.controller() })

  it('does nothing special', () => expect(vm).toBeTruthy())
})
