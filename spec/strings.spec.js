const strings = require('../app/services/strings')

describe('strings', () => {
  describe('get', () => {
    it('returns a text translation with parameter interpolation', () => {
      expect(strings.get('test', {foo: 'abc', bar: 'def'}))
        .toBe('a test translation; foo is abc and bar is def')
    })

    describe('when a translation cannot be found', () => {
      it('throws an error', () => {
        expect(() => strings.get('nonexistent')).toThrow()
      })
    })
  })
})
