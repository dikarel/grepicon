const browser = require('../app/components/browser')
const iconRepo = require('../app/services/iconRepo')
const Promise = require('bluebird')

describe('browser', () => {
  let vm

  beforeEach(() => { vm = browser.controller() })

  it('displays the empty-state message', () => {
    expect(vm.emptyStateMessageVisible()).toBe(true)
  })

  it('hides the no-results message', () => {
    expect(vm.noResultsMessageVisible()).toBe(false)
  })

  describe('when there are search results', () => {
    beforeEach(() => vm.searchResults([
      {name: 'test-1', svg: '<svg></svg>'},
      {name: 'test-2', svg: '<svg></svg>'}
    ]))

    it('displays search result entries', () => {
      expect(vm.searchResults()[0]).toBeTruthy()
      expect(vm.searchResults()[1]).toBeTruthy()
    })

    it('each search result displays a humanized name', () => {
      expect(vm.searchResults()[0].name()).toBe('test 1')
      expect(vm.searchResults()[1].name()).toBe('test 2')
    })

    it('each search result comes with a thumbnail', () => {
      expect(vm.searchResults()[0].thumbnail()).toContain('<svg')
      expect(vm.searchResults()[1].thumbnail()).toContain('<svg')
    })

    it('hides the empty-state message', () => {
      expect(vm.emptyStateMessageVisible()).toBe(false)
    })

    it('hides the no-results message', () => {
      expect(vm.noResultsMessageVisible()).toBe(false)
    })
  })

  describe('when searching without results', () => {
    beforeEach(() => {
      vm.searchResults([])
      vm.searchQuery('test')
    })

    it('displays a no-results message', () => {
      expect(vm.noResultsMessageVisible()).toBe(true)
    })

    it('hides the empty-state message', () => {
      expect(vm.emptyStateMessageVisible()).toBe(false)
    })
  })

  describe('when the search query has been typed in', () => {
    const mockResults = [{}, {}, {}]
    let clock

    beforeEach(() => {
      spyOn(iconRepo, 'search').andReturn(new Promise((resolve) => resolve(mockResults)))
      spyOn(vm, 'searchResults').andCallThrough()
      clock = useFakeTimers()
      vm.searchQuery('test')
    })

    afterEach(() => {
      restoreSpies()
      clock.restore()
    })

    it('does not immediately trigger a search', () => {
      expect(iconRepo.search).toNotHaveBeenCalled()
    })

    describe('after 300ms', () => {
      beforeEach(() => clock.tick(300))

      it('triggers a search', () => {
        expect(iconRepo.search).toHaveBeenCalledWith('test')
      })

      it('loads results', () => {
        expect(vm.searchResults).toHaveBeenCalledWith(mockResults)
      })

      describe('if the search query then cleared', () => {
        beforeEach(() => vm.searchQuery(''))

        it('should clear the results', () => {
          expect(vm.searchResults().length).toBe(0)
        })

        it('should display the empty-state message', () => {
          expect(vm.emptyStateMessageVisible()).toBe(true)
        })
      })
    })
  })
})
